import mongoose from 'mongoose';

import Pokemon from './PokemonSchema';

const connectDb = () => {
    return mongoose.connect('mongodb://localhost:27017/node-express-mongodb-server');
};
const models = { Pokemon };
export { connectDb };
export default models;
