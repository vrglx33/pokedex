import mongoose from 'mongoose';

const pokemonSchema = new mongoose.Schema({
    name: {
        type: String,
        unique: true,
    },
    image: {
        type: String,
        unique: true,
    },
});
const Pokemon = mongoose.model('Pokemon', pokemonSchema);

export default Pokemon;
