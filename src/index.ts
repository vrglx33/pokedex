import express from 'express';
import { PORT } from './config/constants';
import { pokemonRouter } from './routes';
import models, { connectDb } from './dto/MongoClient';

const app = express();
app.use(express.json());

app.use('/pokemon', pokemonRouter);

connectDb().then(async () => {
    app.listen(PORT, () => {
        console.log(`Server is listening on port ${PORT} and ready to consume the pokeapi!`);
    });
});
