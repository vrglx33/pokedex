import express, { Request, Response } from 'express';
import { pokemonController } from '../../controllers';

export const router = express.Router({
    strict: true
});

router.post('/', (req: Request, res: Response) => {
    pokemonController.create(req, res);
});

router.get('/', (req: Request, res: Response) => {
    pokemonController.read(req, res);
});

router.patch('/', (req: Request, res: Response) => {
    pokemonController.update(req, res);
});

router.delete('/', (req: Request, res: Response) => {
    pokemonController.delete(req, res);
});
