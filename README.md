# Pokedex

## Technology Stack
 - Typescript
 - Express
 - Node 12
 - MongoDB

## Get Started
 -  you may need to install mongoDB son here's the link: https://docs.mongodb.com/manual/installation/
 - `npm install`
 - `npm run dev`
 
## Summary

The pokedex is a classic javascript exercise, the idea is consume the PokeAPI and create some endpoints to show and edit data, we'll create a sync task to get the updated data from the pokeAPI and store it in our database.-
the initial goals of this exercise are:
 -  Give to the API the ability to edit/delete/create Pokemons
 -  Give to the API the ability to sync with the pokeAPI and update the pokemons in the current database
 -  Modify the code following the good practices of Typescript and add a linter
 -  Bonus: create a gitlab-ci to deploy the project in zeit.now
 
Enjoy coding!!!
